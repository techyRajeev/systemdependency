## System Dependency App Application 

- [Table of content](#table-of-content)
  * [Introduction](#introduction)
    + [Instructions for Getting Started](#getting-started)
      - [Prerequisites](#prerequisites)
      - [Installing and Setup](#installing)
  * [Running the tests](#running-the-tests)
      - [Unit Testing](#unit-testing)
      - [Regression Testing](#regression-testing)
  * [Running the application](#running-the-application)
       - [Using console input](#using-console-input)
       - [Using file based input](#using-file-input) 
 - [Author](#author)


## Introduction

System Dependency App is an automated dependency installation system.  
It allows customers to install System Dependency App without human intervention.

### Instructions for Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

#### Prerequisites

For running this project, you need to have *Java 8* and latest *Gradle 5.5.1* at least.

For checking which versions you have, you can run the following commands:

```
java --version

gradle --version
```

For gradle installation, Please refer official documentation  - [*Gradle Install*](https://gradle.org/install/)

#### Installing and setup

A step by step series of examples that tell you how to get a the application up and running

I. Clone the repository 

II. goto directory containing the project root directory 
```
cd sys_dep
```

III. Script `bin/setup​` will install all dependencies and compile the
     code and then run all unit test suite. Run below command
```
sys_dep$ ./bin/setup
```
 

### Built With

* [Gradle](https://gradle.org/) - Dependency Management
* [Java](https://java.com/) - Programming language used 
* [JUnit4](https://junit.org/) - Unit Testing Framework 
 
### Created By

* **Rajeev Mishra** - *System Dependency App Application* - [System Dependency App]
