package com.app.depsystemapp.constants;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SystemDependencyConstantsTest {

    @Before
    public void init() {
        new CmdConstants();
        new MessageConstants();
    }

    @Test
    public void dependMessageShouldBeCorrect() {
        Assert.assertEquals("DEPEND", CmdConstants.DEPEND);
    }

    @Test
    public void installMessageShouldBeCorrect() { Assert.assertEquals("INSTALL", CmdConstants.INSTALL);
    }

    @Test
    public void listMessageShouldBeCorrect() {
        Assert.assertEquals("LIST", CmdConstants.LIST);
    }


    @Test
    public void removeMessageShouldBeCorrect() {
        Assert.assertEquals("REMOVE", CmdConstants.REMOVE);
    }


    @Test
    public void exitMessageShouldBeCorrect() {
        Assert.assertEquals("EXIT", CmdConstants.EXIT);
    }

    @Test
    public void operationNotSupportedMessageShouldBeCorrect() {
        Assert.assertEquals("Operation %s not supported", MessageConstants.OPERATION_NOT_SUPPORTED);
    }

    @Test
    public void noSysDepCreatedMessageShouldBeCorrect() {
        Assert.assertEquals("System Dependency App has not been created yet", MessageConstants.sys_dep_NOT_CREATED_MESSAGE );
    }


    @Test
    public void InvalidCommandMessageShouldBeCorrect() {
        Assert.assertEquals("Invalid command provided!", MessageConstants.INVALID_COMMAND);
    }

    @Test
    public void NotFoundMessageShouldBeCorrect() {
        Assert.assertEquals("Not found", MessageConstants.NOT_FOUND);
    }
}