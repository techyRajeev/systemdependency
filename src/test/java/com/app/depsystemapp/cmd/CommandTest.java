package com.app.depsystemapp.cmd;

import com.app.depsystemapp.constants.CmdConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CommandTest  {
    private final ByteArrayOutputStream outContent	= new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldCreateAndReturnCommandInstance() {
        Command command = new Command(CmdConstants.INSTALL,1, (args) -> {});
        assertNotNull(command);
        assertEquals(CmdConstants.INSTALL, command.getCommandName());
    }

    @Test
    public void shouldExpectCorrectArgsForCommand() {
        Command command = new Command(CmdConstants.INSTALL,1, (args)->{});
        assertEquals(1, command.getExpectedArguments());
    }

    @Test
    public void shouldExecuteCommandCorrectly() {
        CmdBinder cmd = (args)-> {
            System.out.println(args.length);};

        Command command = new Command(CmdConstants.INSTALL,1, cmd);
        command.getCommandTo().execute(new String[]{});
        assertEquals("0\n", outContent.toString());
    }

}