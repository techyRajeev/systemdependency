package com.app.depsystemapp.cmd;

import com.app.depsystemapp.cmd.service.CmdExecutionService;
import com.app.depsystemapp.cmd.service.SystemDependencyCmdExecutionService;
import com.app.depsystemapp.constants.CmdConstants;
import com.app.depsystemapp.constants.MessageConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class SystemDependencyCmdExecutionServiceTest {
    private final ByteArrayOutputStream outContent	= new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void shouldCreateAndReturnParkingCmdServiceInstance() {
        CmdExecutionService cmdExecutionService = new SystemDependencyCmdExecutionService();
        assertNotNull(cmdExecutionService);
    }

    @Test
    public void shouldBeInstanceOfCmdExecutionService() {
        SystemDependencyCmdExecutionService cmdExecutionService = new SystemDependencyCmdExecutionService();
        assertNotNull(cmdExecutionService instanceof CmdExecutionService);
    }

    @Test
    public void shouldAddAllParkingCommands() {
        CmdExecutionService cmdExecutionService = new SystemDependencyCmdExecutionService();
        assertTrue(cmdExecutionService.validateCommand(CmdConstants.LIST));
        assertTrue(cmdExecutionService.validateCommand(CmdConstants.REMOVE));
        assertTrue(cmdExecutionService.validateCommand(CmdConstants.INSTALL));
        assertTrue(cmdExecutionService.validateCommand(CmdConstants.DEPEND));
    }

    @Test
    public void shouldValidateAddAllParkingCommandsArguments() {
        CmdExecutionService cmdExecutionService = new SystemDependencyCmdExecutionService();
        assertTrue(cmdExecutionService.validateCommandArgs(CmdConstants.INSTALL, 1));
        assertTrue(cmdExecutionService.validateCommandArgs(CmdConstants.REMOVE, 2));
        assertTrue(cmdExecutionService.validateCommandArgs(CmdConstants.DEPEND, 1));
        assertTrue(cmdExecutionService.validateCommandArgs(CmdConstants.LIST, 0));
    }

    @Test
    public void shouldExecuteCommandCorrectly() {
        CmdExecutionService cmdExecutionService = new SystemDependencyCmdExecutionService();
        cmdExecutionService.execute(CmdConstants.INSTALL, new String[]{"foo"});
        assertEquals(String.format(MessageConstants.DEPENDENCY_INSTALLATION_MESSAGE+"\n", "foo"), outContent.toString());
    }

}