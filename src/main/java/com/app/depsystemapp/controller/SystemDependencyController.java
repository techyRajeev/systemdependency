package com.app.depsystemapp.controller;

import com.app.depsystemapp.constants.MessageConstants;
import com.app.depsystemapp.models.DependencyItem;

import java.util.*;
import java.util.stream.Collectors;

import static com.app.depsystemapp.constants.MessageConstants.DEPENDENCY_INSTALLATION_MESSAGE;
import static com.app.depsystemapp.constants.MessageConstants.INVALID_COMMAND_ARGUMENTS;

public class SystemDependencyController  {
    private static SystemDependencyController SysDep = null;
    private Map<DependencyItem, Set<DependencyItem>> adjListMap = new HashMap<>();
    private Map<DependencyItem, Integer> inDegrees = new HashMap<>();
    private Map<String, DependencyItem> allItems = new HashMap<>();
    private Set<DependencyItem> installedItems = new HashSet<>();

    // static method to get the singleton instance of SysDep
    public static SystemDependencyController getInstance() {
        if (SysDep == null) {
            //synchronized block to remove overhead
            synchronized (SystemDependencyController.class) {
                if(SysDep ==null) {
                    // if instance is null, initialize
                    SysDep = new SystemDependencyController();
                }

            }
        }
        return SysDep;
    }

    public boolean validateArgs(String args[], int length) {
        return (args == null || args.length < length)
                ? false
                : true;
    }

    public void createDependencies(String args[]) {
        if(!validateArgs(args, 1)) {
            throw new IllegalArgumentException(INVALID_COMMAND_ARGUMENTS);
        }
        DependencyItem dependencyItem = new DependencyItem(args[0]);
        HashSet<DependencyItem> dependencies = new HashSet<>();
        for (int i = 1; i < args.length; i++) {
            dependencies.add(new DependencyItem(args[i]));
        }

        for (DependencyItem di : dependencies) {
            inDegrees.put(di, inDegrees.getOrDefault(di, 0)+1);
        }

        adjListMap.computeIfAbsent(dependencyItem, k-> new HashSet<>()).addAll(dependencies);
    }

    public boolean checkAllDependenciesInstalled(Set<DependencyItem> dependencyItems) {
        return dependencyItems
                .stream()
                .filter(di->!installedItems.contains(di)).count() == 0;
    }

    public void installDependencies(Set<DependencyItem> dependencyItems) {

        if(dependencyItems == null || dependencyItems.size() == 0
                || checkAllDependenciesInstalled(dependencyItems))
            return;

        for (DependencyItem di: dependencyItems) {
            if(!installedItems.contains(di)) {
                Set<DependencyItem> dependencies = adjListMap.get(di);
                installDependencies(dependencies);
                System.out.println(String.format(DEPENDENCY_INSTALLATION_MESSAGE, di));
                installedItems.add(di);
            }
        }

    }

    public void install(String[] args) {
        if(!validateArgs(args, 1)) {
            throw new IllegalArgumentException(INVALID_COMMAND_ARGUMENTS);
        }


        DependencyItem installItem = new DependencyItem(args[0]);

        if(installedItems.contains(installItem)) {
            System.out.println(String.format("%s is already installed", installItem));
            return;
        }

        Set<DependencyItem> dependencyItems = new HashSet<>();
        dependencyItems.add(installItem);
        installDependencies(dependencyItems);
    }

    public boolean checkAllDependenciesRemoved(Set<DependencyItem> dependencyItems) {
        return dependencyItems
                .stream()
                .filter(di->installedItems.contains(di)).count() == 0;
    }

    public void removeDependencies(Set<DependencyItem> dependencyItems, Set<DependencyItem> removableItems) {

        if(dependencyItems == null || dependencyItems.size() == 0
                || checkAllDependenciesRemoved(dependencyItems))
            return;

        for (DependencyItem di: dependencyItems) {
            if(installedItems.contains(di) && inDegrees.getOrDefault(di, 0) <= 1) {
                Set<DependencyItem> dependencies = adjListMap.get(di);
                removeDependencies(dependencies, removableItems);
                removableItems.add(di);
            }
        }
    }

    public synchronized void removeDependency(String[] args) {
        if(!validateArgs(args, 1)) {
            throw new IllegalArgumentException(INVALID_COMMAND_ARGUMENTS);
        }

        DependencyItem removeItem = new DependencyItem(args[0]);
        if(!installedItems.contains(removeItem)) {
            System.out.println(String.format("%s not installed", removeItem));
            return;
        }

        if(inDegrees.getOrDefault(removeItem, 0) <= 1) {
            Set<DependencyItem> removeItems = new HashSet<>();
            removeItems.add(removeItem);
            Set<DependencyItem> removableItems = new HashSet<>();
            removeDependencies(removeItems, removableItems);
            removableItems
                        .stream()
                        .map(removableItem -> {
                            installedItems.remove(removableItem);
                            return String.format("Removing %s", removableItem );
                        })
                        .forEachOrdered(System.out::println);
            updateIndegrees(removableItems);
        } else {
            System.out.println(String.format("%s still needed", removeItem));
        }

    }

    public void updateIndegrees(Set<DependencyItem> removedItems) {
        for (DependencyItem di: removedItems) {
            adjListMap
                    .computeIfPresent(di,
                            (k,v) -> v.stream()
                                    .map(i -> { inDegrees.computeIfPresent(i, (ki, vi) -> vi-1); return i;})
                                    .collect(Collectors.toSet()));
            adjListMap.remove(di);
            inDegrees.remove(di);
        }
    }


    public void listDependencies(String []args) {
        installedItems.stream().forEachOrdered(System.out::println);
    }
}
