package com.app.depsystemapp.constants;

public final class MessageConstants {
    public static final String OPERATION_NOT_SUPPORTED = "Operation %s not supported";
    public static final String sys_dep_NOT_CREATED_MESSAGE = "System Dependency App has not been created yet";
    public static final String DEPENDENCY_INSTALLATION_MESSAGE = "Installing %s";
    public static final String INVALID_COMMAND = "Invalid command provided!";
    public static final String INVALID_COMMAND_ARGUMENTS = "Invalid command input provided!";
    public static final String NOT_FOUND = "Not found";
    public MessageConstants(){

    }
}