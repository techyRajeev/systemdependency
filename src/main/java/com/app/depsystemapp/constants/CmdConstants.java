package com.app.depsystemapp.constants;
public final class CmdConstants {
   public static final String   EXIT                             = "EXIT";
   public static final String   DEPEND                             = "DEPEND";
   public static final String	INSTALL							 = "INSTALL";
   public static final String	REMOVE							 = "REMOVE";
   public static final String	LIST							 = "LIST";

   public CmdConstants() {}
}