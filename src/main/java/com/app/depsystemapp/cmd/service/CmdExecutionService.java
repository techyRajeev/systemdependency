package com.app.depsystemapp.cmd.service;

import com.app.depsystemapp.constants.MessageConstants;
import com.app.depsystemapp.cmd.Command;

import java.util.HashMap;

public abstract class CmdExecutionService {
    final HashMap<String, Command> commandMap = new HashMap<>();

    public boolean validateCommand(String commandName) {
        Command cmd = commandMap.get(commandName.trim());
        return cmd == null ? false : true;
    }

    public boolean validateCommandArgs(String commandName, int argsLength) {
        Command cmd = commandMap.get(commandName.trim());
        if(cmd == null) return false;
        return argsLength >= cmd.getExpectedArguments();
    }

    public void register(Command command) {
        commandMap.put(command.getCommandName(), command);
    }

    public void execute(String commandName, String []args) {
        Command command = commandMap.get(commandName);
        if (command == null) {
            throw new IllegalStateException(String.format(MessageConstants.OPERATION_NOT_SUPPORTED, commandName));
        }
        command.getCommandTo().execute(args);
    }

    public abstract void initCommands();
    public abstract void printCommandManual();
    public abstract void cleanUp();
}
