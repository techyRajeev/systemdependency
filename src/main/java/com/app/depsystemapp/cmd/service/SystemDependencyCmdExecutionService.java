package com.app.depsystemapp.cmd.service;

import com.app.depsystemapp.constants.CmdConstants;
import com.app.depsystemapp.cmd.Command;
import com.app.depsystemapp.controller.SystemDependencyController;

public class SystemDependencyCmdExecutionService extends CmdExecutionService {
    private final SystemDependencyController SystemDependencyController = com.app.depsystemapp.controller.SystemDependencyController.getInstance();

    public SystemDependencyCmdExecutionService() {
        initCommands();
    }

    public void initSystemCommands() {
        register(new Command(CmdConstants.DEPEND, 1, SystemDependencyController::createDependencies));
        register(new Command(CmdConstants.INSTALL, 1, SystemDependencyController::install));
        register(new Command(CmdConstants.REMOVE, 1, SystemDependencyController::removeDependency));
        register(new Command(CmdConstants.LIST, 0, SystemDependencyController::listDependencies));
    }

    @Override
    public void initCommands() {
        initSystemCommands();
    }

    @Override
    public void printCommandManual() {
        StringBuffer buffer = new StringBuffer();
        buffer = buffer
                .append("---------------------------------------------------------------------------------------------")
                .append("\n")
                .append("   <=============================>    System Dependency App   <==============================>")
                .append("\n")
                .append("---------------------------------------------------------------------------------------------")
                .append("\n")
                .append(String.format("*) %s \t\t\t\t\t-- enter '%s' to end.", CmdConstants.EXIT, CmdConstants.EXIT))
                .append("\n")
                .append("-----------------------------------------------------------------------------------------------");

        System.out.println(buffer.toString());
    }
    public void cleanUp() {
    }
}
