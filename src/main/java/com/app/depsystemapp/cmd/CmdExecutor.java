package com.app.depsystemapp.cmd;

import com.app.depsystemapp.cmd.service.CmdExecutionService;
import com.app.depsystemapp.constants.CmdConstants;
import com.app.depsystemapp.constants.MessageConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class CmdExecutor {
    private Scanner inputReader;
    private CmdExecutionService service;

    private CmdExecutor(Scanner inputReader) {
        this.inputReader = inputReader;
    }

    public static CmdExecutor getConsoleBasedCmdExecutor() {
        return new CmdExecutor(new Scanner(System.in));
    }

    public static CmdExecutor getFileBasedCmdExecutor(String filePath) throws FileNotFoundException {
        File inputFile = new File(filePath);
        return new CmdExecutor(new Scanner(inputFile));
    }

    private String [] getCmdAndArgs(String input) {
        String [] response = input.split(" ");
        return response;
    }

    private String [] getArgs(String [] cmdWithArgs) {
        String [] args = new String[] {};
        if(cmdWithArgs.length > 1) {
         args = Arrays.copyOfRange(cmdWithArgs, 1, cmdWithArgs.length);
        }
        return args;
    }

    public void startCmdProcessing() throws Exception {
        String input;
        try {
            while(inputReader.hasNext()) {
                input = inputReader.nextLine().trim();
                if (input.equalsIgnoreCase(CmdConstants.EXIT)) {
                    break;
                } else {
                    validateThenExecuteCommand(input);
                }
            }
        } catch (Exception e) {
            throw new Exception("Error", e);
        } finally {
            cleanUp();
        }
    }

    public void setExecutionService(CmdExecutionService service) {
        this.service = service;
    }

    public void validateThenExecuteCommand(String input) throws Exception {
        String [] commandWithArgs = getCmdAndArgs(input);
        String command = commandWithArgs[0];
        String []args = getArgs(commandWithArgs);

        if(!this.service.validateCommand(command)){
            service.printCommandManual();
            System.out.println(MessageConstants.INVALID_COMMAND);
            return;
        }

        if(!this.service.validateCommandArgs(command, args.length)){
            service.printCommandManual();
            System.out.println(MessageConstants.INVALID_COMMAND_ARGUMENTS);
            return;
        }

        try {
            this.service.execute(command, args);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Some error occurred while cmd execution!", e);
        }
    }

    public void cleanUp() {
        if (inputReader != null) {
            inputReader.close();
        }
        if(service != null) {
            service.cleanUp();
        }
    }
}
