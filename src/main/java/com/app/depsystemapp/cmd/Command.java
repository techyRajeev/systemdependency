package com.app.depsystemapp.cmd;

public class Command {
    private CmdBinder commandTo;
    private int expectedArguments;
    private String commandName;

    public String getCommandName() {
        return commandName;
    }
    public int getExpectedArguments() {
       return expectedArguments;
    }

    public CmdBinder getCommandTo() {
        return commandTo;
    }

    public Command(String commandName, int requiredArgs, CmdBinder cmd) {
        this.commandName = commandName;
        this.expectedArguments = requiredArgs;
        this.commandTo = cmd;
    }
}