package com.app.depsystemapp.cmd;

public interface CmdBinder {
    void execute(String[] args);
}
