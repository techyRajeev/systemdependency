package com.app.depsystemapp;

import com.app.depsystemapp.constants.MessageConstants;
import com.app.depsystemapp.cmd.CmdExecutor;
import com.app.depsystemapp.cmd.service.SystemDependencyCmdExecutionService;

public class App {

    public static void main(String[] args) {
        CmdExecutor executor = null;
        try {
            switch (args.length) {
                case 0: {
                    executor = CmdExecutor.getConsoleBasedCmdExecutor();
                    break;
                }
                case 1: {
                    executor = CmdExecutor.getFileBasedCmdExecutor(args[0]);
                    break;
                }
                default: System.out.println(MessageConstants.INVALID_COMMAND);
            }
            if(executor != null) {
                executor.setExecutionService(new SystemDependencyCmdExecutionService());
                executor.startCmdProcessing();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } finally {
            if(executor !=null) {
                executor.cleanUp();
            }
        }
    }

}
